﻿using UnityEngine;

public class TipsController : MonoBehaviour
{
    public static TipsController I;
    [SerializeField] private string tipsPrefKey;
    [SerializeField] private GameObject tipsWindow;
    private bool IsPassedTips => PlayerPrefs.GetInt(tipsPrefKey) == 1;
    private void Awake()
    {
        if (I == null)
        {
            I = this;
        }

        SetActive(false);
    }

    private void Start()
    {
        ShowStartTips();
    }

    public void PassTips()
    {
        PlayerPrefs.SetInt(tipsPrefKey, 1);
    }

    public void ShowStartTips()
    {
        //if (!IsPassedTips)
        //{
            SetActive(true);
            PassTips();
        //}
    }

    public void SetActive(bool isActive)
    {
        tipsWindow.SetActive(isActive);
    }
}
