﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TagSaveController : BaseSaveController<TagContainer>
{
    public static TagSaveController I;
    protected override string SaveFileName => "tag.tg";

    private void Awake()
    {
        if (I == null)
        {
            I = this;
        }
    }
}
