﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SearchManager : MonoBehaviour
{
    public static event Action<List<long>> OnSearchTextUpdated;
    private List<TagElement> tagElements = new List<TagElement>();
    [SerializeField] private Transform holder;
    private int selectedId;
    public void OnTextUpdatedHandler(string s)
    {
        var data = SessionController.I.GetNotes();
        var ids = new List<long>();
        foreach (var note in data.notes)
        {
            if (note.title.Contains(s) || note.description.Contains(s))
            {
                ids.Add(note.id);
            }
        }
        OnSearchTextUpdated?.Invoke(ids);
    }

    public void OnTagClickHandler(int id)
    {
        if (id == selectedId)
        {
            tagElements.FirstOrDefault(x => x.Id == id)?.SetSelect(false);
            selectedId = -1;
            OnSearchTextUpdated?.Invoke(new List<long>());
            return;

        }

        foreach (var tagElement in tagElements)
        {
            if (tagElement.Id == id)
            {
                selectedId = id;
                tagElement.SetSelect(true);

            }
            else
            {
                tagElement.SetSelect(false);

            }
        }

        var data = SessionController.I.GetNotes();
        var ids = new List<long>();

        foreach (var note in data.notes)
        {
            if (note.tag.id.Equals(id))
            {
                ids.Add(note.id);
            }
        }

        OnSearchTextUpdated?.Invoke(ids);
    }

    private void ClearAllTags()
    {
        foreach (var tag in tagElements.ToArray())
        {
            Destroy(tag.gameObject);
        }

        tagElements.Clear();
    }

    private void Awake()
    {
        InitTags();
    }
    private void InitTags()
    {
        ClearAllTags();
        for (var i = 0; i < TagController.I.TagContainer.tags.Count; i++)
        {
            var data = TagController.I.TagContainer.tags[i];
            if (i == tagElements.Count)
            {
                var elem = Instantiate(TagController.I.TagPrefab);
                elem.transform.SetAsLastSibling();
                elem.transform.SetParent(holder, false);
                elem.transform.localRotation = Quaternion.identity;
                elem.transform.localScale = Vector3.one;
                tagElements.Add(elem);
            }
            tagElements[i].OnElementClick -= OnTagClickHandler;
            tagElements[i].OnElementClick += OnTagClickHandler;
            tagElements[i].Init(data);
        }
    }

}
