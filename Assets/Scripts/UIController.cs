﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    [SerializeField] private List<GameObject> noteEditContent;
    [SerializeField] private List<GameObject> mainContent;
    [SerializeField] private NoteContent noteContent;

    public static event Action<WindowType> OnWindowOpen;

    public static UIController I;

    public NoteContent Content => noteContent;

    private void Awake()
    {
        if (I == null)
        {
            I = this;
        }
    }

    private void Start()
    {
        OpenMainContentScreen();
        NoteController.OnNoteStateUpdate += OpenMainContentScreen;
        NoteController.OnNoteDelete += OpenMainContentScreenAfterDelete;
    }

    public void OpenAddNoteScreen()
    {
        OpenScreen(WindowType.NoteEdit);
        NoteController.I.CreateNewNote();
    }

    public void OpenUpdateExistingNote(Note note)
    {
        OpenScreen(WindowType.NoteEdit);
        NoteController.I.UpdateExistingNote(note);
    }

    public void OpenMainContentScreen()
    {
        OpenScreen(WindowType.MainContent);
        Content.InitNotes();
    }

    public void OpenMainContentScreenAfterDelete(long id)
    {
        OpenScreen(WindowType.MainContent);
        Content.RemoveNote(id);
    }

    public void OpenScreen(WindowType type)
    {
        noteEditContent.ForEach(x => x.SetActive(type == WindowType.NoteEdit));
        mainContent.ForEach(x => x.SetActive(type == WindowType.MainContent));
        OnWindowOpen?.Invoke(type);
    }

    private void OnDestroy()
    {
        NoteController.OnNoteStateUpdate -= OpenMainContentScreen;
        NoteController.OnNoteDelete -= OpenMainContentScreenAfterDelete;
    }
}

public enum WindowType
{
    NoteEdit,
    MainContent
}
