﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NoteController : MonoBehaviour
{
    [SerializeField] private NoteWindow noteWindow;
    public static event Action OnNoteSave;
    public static event Action<long> OnNoteDelete;
    public static event Action OnNoteStateUpdate;
    public static NoteController I;

    public Note CurrentNote { get; set; }

    private void Awake()
    {
        if (I == null)
        {
            I = this;
        }
        noteWindow.OnDescriptionUpdate += OnDescriptionUpdateHandler;
        noteWindow.OnTitleUpdate += OnTitleUpdateHandler;
        noteWindow.OnTagClick += OnTagClickHandler;
    }

    private void OnTagClickHandler(Tag tag)
    {
        CurrentNote.SetTag(tag);
    }

    private void OnTitleUpdateHandler(string s)
    {
        CurrentNote.title = s;
    }

    private void OnDescriptionUpdateHandler(string s)
    {
        CurrentNote.description = s;
    }

    public void SaveNote()
    {
        SessionController.I.AddNote(CurrentNote);
        OnNoteStateUpdate?.Invoke();
    }

    public void CreateNewNote()
    {
        CurrentNote = new Note();
        noteWindow.UpdateView(CurrentNote);
    }

    public void UpdateExistingNote(Note note)
    {
        CurrentNote = new Note(note);
        noteWindow.UpdateView(note);
    }

    public void RemoveNote()
    {
        OnNoteDelete?.Invoke(CurrentNote.id);
        SessionController.I.RemoveNote(CurrentNote);
    }

    public void RemoveNote(long id)
    {
        SessionController.I.RemoveNote(SessionController.I.GetNoteById(id));
    }
}
