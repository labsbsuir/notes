﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class Note
{
    public string title;
    public string description;
    public Tag tag;
    public long id;

    public Note(string title, string description, Tag tag, int id)
    {
        this.title = title;
        this.description = description;
        this.tag = tag;
        this.id = id;
    }

    public Note(Note note)
    {
        title = note.title;
        description = note.description;
        tag = note.tag;
        id = note.id;
    }

    public Note()
    {
        id = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
        tag = new Tag();
    }

    public void SetTag(Tag tag)
    {
        if (this.tag.id > 0)
        {
            if (tag.id == this.tag.id)
            {
                Debug.Log("equdaskdsa");
                this.tag = new Tag();
            }
            else
            {
                this.tag = tag;
            }
        }
        else
        {
            Debug.Log("nll");
            this.tag = tag;
        }
    }
}

[Serializable]
public class NoteContainer
{
    public List<Note> notes = new List<Note>();

    public void SaveNote(Note note)
    {
        if (notes.Any(x => x.id == note.id))
        {
            var n = notes.Find(x => x.id == note.id);
            n.description = note.description;
            n.tag = note.tag;
            n.title = note.title;
        }
        else
        {
            notes.Add(note);
        }
    }

    public void RemoveNote(Note note)
    {
        var noteToDelete = notes.FirstOrDefault(x => x.id == note.id);
        if (noteToDelete != null)
        {
            Debug.Log($"Removed note {note.id}");
            notes.Remove(noteToDelete);
        }
    }

    public Note GetNoteById(long id)
    {
        return notes.FirstOrDefault(x => x.id == id);
    }
}