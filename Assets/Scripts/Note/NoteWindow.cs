﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class NoteWindow : MonoBehaviour
{
    [SerializeField] private InputField titleField;
    [SerializeField] private InputField descriptionField;
    [SerializeField] private ScrollRect scrollView;

    private List<TagElement> tagElements = new List<TagElement>();
    public event Action<string> OnTitleUpdate;
    public event Action<string> OnDescriptionUpdate;
    public event Action<Tag> OnTagClick;

    private int currentSelectedId;

    public void UpdateView(Note note)
    {
        titleField.text = note.title;
        descriptionField.text = note.description;
        InitTags(note.tag);
    }

    public void OnTitleUpdateHandler(string s)
    {
        OnTitleUpdate?.Invoke(s);
    }

    public void OnDescriptionUpdateHandler(string s)
    {
        OnDescriptionUpdate?.Invoke(s);
    }

    private void InitTags(Tag tag)
    {
        for (var i = 0; i < TagController.I.TagContainer.tags.Count; i++)
        {
            var data = TagController.I.TagContainer.tags[i];
            if (i == tagElements.Count)
            {
                var elem = Instantiate(TagController.I.TagPrefab);
                elem.transform.SetAsLastSibling();
                elem.transform.SetParent(scrollView.content, false);
                elem.transform.localRotation = Quaternion.identity;
                elem.transform.localScale = Vector3.one;
                tagElements.Add(elem);
            }

            tagElements[i].OnElementClick -= OnTagClickHandler;
            tagElements[i].OnElementClick += OnTagClickHandler;
            tagElements[i].Init(data);
            if (tag != null)
            {
                if (data.id == tag.id)
                {
                    currentSelectedId = tag.id;
                    tagElements[i].SetSelect(true);
                }
                else
                {
                    tagElements[i].SetSelect(false);
                }

            }
        }
    }

    private void OnTagClickHandler(int id)
    {
        if (id == currentSelectedId)
        {
            tagElements.FirstOrDefault(x => x.Id == id)?.SetSelect(false);
            currentSelectedId = -1;
            OnTagClick?.Invoke(TagController.I.GetTagById(id));
            return;
        }
        foreach (var tagElement in tagElements)
        {
            if (tagElement.Id == id)
            {
                tagElement.SetSelect(true);
                currentSelectedId = tagElement.Id;

            }
            else
            {
                tagElement.SetSelect(false);
            }
        }

        OnTagClick?.Invoke(TagController.I.GetTagById(id));
    }

}
