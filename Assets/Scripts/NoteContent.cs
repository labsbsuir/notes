﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class NoteContent : MonoBehaviour
{
    [SerializeField] private List<NoteElement> noteElements = new List<NoteElement>();
    [SerializeField] private NoteElement notePrefab;
    [SerializeField] private ScrollRect scrollView;

    public ScrollRect ScrollView => scrollView;

    private void Awake()
    {
        SearchManager.OnSearchTextUpdated += SearchManager_OnSearchTextUpdated;
    }

    private void SearchManager_OnSearchTextUpdated(List<long> obj)
    {
        if (obj.Count == 0)
        {
            foreach (var elem in noteElements)
            {
                elem.gameObject.SetActive(true);
            }

            return;
        }
        foreach (var elem in noteElements)
        {
            elem.gameObject.SetActive(obj.Any(x => x == elem.Id));
        }
    }

    public void InitNotes()
    {
        var noteData = SessionController.I.GetNotes();
        if (noteData == null) return;
        for (int i = 0; i < noteData.notes.Count; i++)
        {
            var note = noteData.notes[i];
            if (i == noteElements.Count)
            {
                var elem = Instantiate(notePrefab, ScrollView.content, false);
                SetDefaultState(elem.transform);
                noteElements.Add(elem);
            }

            gameObject.SetActive(false);
            gameObject.SetActive(true);
            noteElements[i].OnElementClick += OnElementClickHandler;
            noteElements[i].Init(note);
        }
    }

    public void RemoveNote(long id)
    {
        var note = noteElements.FirstOrDefault(x => x.Id == id);
        if (note != null)
        {
            noteElements.Remove(note);
            Destroy(note.gameObject);
        }
    }

    private void OnElementClickHandler(long id)
    {
        UIController.I.OpenUpdateExistingNote(SessionController.I.GetNoteById(id));
    }

    private void SetDefaultState(Transform t)
    {
        t.SetAsLastSibling();
        t.localRotation = Quaternion.identity;
        t.localScale = Vector3.one;
    }
}
