﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class ColorHelper : MonoBehaviour
{
    public static Color HexToColor(string hexString)
    {
        if (hexString.StartsWith("#"))
        {
            hexString = hexString.Substring(1);
        }

        if (hexString.StartsWith("0x"))
        {
            hexString = hexString.Substring(2);
        }

        switch (hexString.Length)
        {
            case 6:
            {
                byte r = byte.Parse(hexString.Substring(0, 2), NumberStyles.HexNumber);
                byte g = byte.Parse(hexString.Substring(2, 2), NumberStyles.HexNumber);
                byte b = byte.Parse(hexString.Substring(4, 2), NumberStyles.HexNumber);

                return new Color32(r, g, b, 255);
            }
            case 8:
            {
                byte r = byte.Parse(hexString.Substring(0, 2), NumberStyles.HexNumber);
                byte g = byte.Parse(hexString.Substring(2, 2), NumberStyles.HexNumber);
                byte b = byte.Parse(hexString.Substring(4, 2), NumberStyles.HexNumber);
                byte a = byte.Parse(hexString.Substring(6, 2), NumberStyles.HexNumber);

                return new Color32(r, g, b, a);
            }
            default:
                Debug.Log($"{hexString} is not a valid color string.");

                return Color.white;
        }
    }
}
