﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NoteElement : MonoBehaviour, IPointerClickHandler, IPointerUpHandler, IPointerDownHandler
{
    [SerializeField] private TextMeshProUGUI title;
    [SerializeField] private TextMeshProUGUI description;
    [SerializeField] private List<TagElement> tags;
    [SerializeField] private Transform tagHolder;

    private long id;
    public float InitialXPosition { get; set; }
    private bool isInteractable = true;
    private float animationTime = 0.1f;

    public long Id => id;
    public event Action<long> OnElementClick;

    public void SetInteractable(bool interaction)
    {
        isInteractable = interaction;
    }

    public void Init(Note note)
    {
        title.text = note.title;
        description.text = note.description;
        id = note.id;
        InitTags(note.tag);
    }

    private void InitTags(Tag tag)
    {
        foreach (var t in tags)
        {
            Destroy(t.gameObject);
        }

        tags.Clear();
        if (tag.id <= 0) return;
        var elem = Instantiate(TagController.I.TagNotePrefab);
        elem.transform.SetAsLastSibling();
        elem.transform.SetParent(tagHolder, false);
        elem.transform.localRotation = Quaternion.identity;
        elem.transform.localScale = Vector3.one;
        elem.Init(tag);
        tags.Add(elem);

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (isInteractable)
        {
            OnElementClick?.Invoke(Id);
        }
    }

    public void ReturnToInitPlace()
    {
        transform.DOLocalMoveX(InitialXPosition, animationTime).OnComplete(() => SetInteractable(true));
    }

    public void MoveToRightCorner()
    {
        transform.DOLocalMoveX(GetComponent<RectTransform>().rect.width + InitialXPosition, animationTime).OnComplete(() => UIController.I.Content.RemoveNote(id));
    }

    public void MoveToLeftCorner()
    {
        transform.DOLocalMoveX(-InitialXPosition, animationTime).OnComplete(() => UIController.I.Content.RemoveNote(id));
    }

    public void OnPointerUp(PointerEventData eventData)
    {

    }

    public void OnPointerDown(PointerEventData eventData)
    {
    }
}
