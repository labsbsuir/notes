﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NoteDeleteGesture : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler/*, IPointerUpHandler, IPointerDownHandler, IInitializePotentialDragHandler*/
{
    [SerializeField] private NoteElement noteElement;
    private ScrollRect parentScrollRect;

    private bool canDrag = true;
    private void Start()
    {
        parentScrollRect = GetComponentInParent<NoteContent>().ScrollView;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (Mathf.Abs(eventData.delta.y) > 0.4f && Mathf.Abs(eventData.delta.x) <= 5f) /*(eventData.delta.y > 0 || eventData.delta.y < 0) && (eventData.delta.x ))*/
        {
            canDrag = false;
            noteElement.SetInteractable(false);
            parentScrollRect.OnBeginDrag(eventData);
            return;
        }
        else
        {
            noteElement.InitialXPosition = transform.localPosition.x;
        }
        noteElement.SetInteractable(false);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!canDrag)
        {
            parentScrollRect.OnEndDrag(eventData);
            noteElement.SetInteractable(true);
            canDrag = true;
            return;
        }
        var diff = noteElement.InitialXPosition - transform.localPosition.x;
        var limit = GetComponent<RectTransform>().rect.width / 1.5f;
        if (Mathf.Abs(diff) > limit)
        {
            NoteController.I.RemoveNote(noteElement.Id);
            if (diff > 0)
            {
                noteElement.MoveToLeftCorner();
            }
            else if (diff <= 0)
            {
                noteElement.MoveToRightCorner();
            }
        }
        else
        {
            noteElement.ReturnToInitPlace();
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (canDrag)
        {
            transform.position += new Vector3(eventData.delta.x, 0);
        }
        else
        {
            parentScrollRect.OnDrag(eventData);
        }
    }
}