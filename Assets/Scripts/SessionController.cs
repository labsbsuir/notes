﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SessionController : MonoBehaviour
{
    [SerializeField] private NoteContainer noteContainer;
    public static SessionController I;

    private void Awake()
    {
        if (I == null)
        {
            I = this;
        }
        noteContainer = NoteSaveController.I.Load();
    }

    public void AddNote(Note note)
    {
        note.title = UpdateNoteTitle(note);
        noteContainer.SaveNote(note);
        SerializeNotes();
    }

    public void RemoveNote(Note note)
    {
        noteContainer.RemoveNote(note);
        SerializeNotes();
    }

    private string UpdateNoteTitle(Note note)
    {
        string date = DateTimeOffset.Parse(DateTimeOffset.UtcNow.ToLocalTime().ToString(), null).ToString("dd-MM-yyyy HH:mm:ss");
        return string.IsNullOrEmpty(note.title) || string.IsNullOrWhiteSpace(note.title)
            ? date
            : note.title;
    }

    public Note GetNoteById(long id)
    {
        return noteContainer.GetNoteById(id);
    }

    public NoteContainer GetNotes()
    {
        return noteContainer;
    }

    public void SerializeNotes()
    {
        string json = JsonUtility.ToJson(noteContainer);
        NoteSaveController.I.Save(json);
    }
}
