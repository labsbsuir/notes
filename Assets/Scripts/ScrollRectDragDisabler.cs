﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ScrollRectDragDisabler : ScrollRect
{

    public override void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("OnBeginDrag");
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("OnEndDrag");

    }

    public override void OnDrag(PointerEventData eventData)
    {
        Debug.Log("OnDrag");

    }
}
