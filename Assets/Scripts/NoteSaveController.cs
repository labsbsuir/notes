﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteSaveController : BaseSaveController<NoteContainer>
{
    public static NoteSaveController I;
    protected override string SaveFileName => "notes.nt";

    private void Awake()
    {
        if (I == null)
        {
            I = this;
        }
    }
}
