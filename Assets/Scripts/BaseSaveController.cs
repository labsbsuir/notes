﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class BaseSaveController<T> : MonoBehaviour where T : new()
{
    protected virtual string SaveFileName { get; set; }

    public void Save(string data)
    {
        var path = Path.Combine(Application.persistentDataPath, SaveFileName);
        File.WriteAllText(path, data);
    }

    public T Load()
    {
        var path = Path.Combine(Application.persistentDataPath, SaveFileName);
        if (File.Exists(path))
        {
            var s = File.ReadAllText(path);
            if (!string.IsNullOrEmpty(s))
            {
                var json = JsonUtility.FromJson<T>(s);
                return json;
            }
        }
        return new T();
    }
}
