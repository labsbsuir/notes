﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TagController : MonoBehaviour
{
    [SerializeField] private TagContainer tagContainer;
    public static TagController I;
    [SerializeField] private TagElement tagPrefab;
    [SerializeField] private TagElement tagNotePrefab;
    public TagContainer TagContainer => tagContainer;

    public TagElement TagPrefab => tagPrefab;
    public TagElement TagNotePrefab => tagNotePrefab;

    private void Awake()
    {
        if (I == null)
        {
            I = this;
        }
    }

    public Tag GetTagById(int id)
    {
        return tagContainer.GetTagById(id);
    }

    public void AddTag(Tag newTag)
    {
        tagContainer.AddTag(newTag);
    }

    public void ClearTags()
    {
        tagContainer.ClearTags();
    }
}

[Serializable]
public class TagContainer
{
    public List<Tag> tags = new List<Tag>();

    public Tag GetTagById(int id)
    {
        return tags.FirstOrDefault(x => x.id == id);
    }

    public void AddTag(Tag newTag)
    {
        tags.Add(newTag);
    }

    public void ClearTags()
    {
        tags.Clear();
    }
}
