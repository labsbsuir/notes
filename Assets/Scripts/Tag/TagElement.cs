﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TagElement : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private Image tagImage;
    [SerializeField] private Image background;
    private int id;
    private bool isSelected;

    public int Id => id;

    public event Action<int> OnElementClick;

    public void Init(Tag tagData)
    {
        if (!string.IsNullOrEmpty(tagData.hexCode))
        {
            tagImage.color = ColorHelper.HexToColor(tagData.hexCode);
        }

        if (tagData.id > 0)
        {
            id = tagData.id;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        isSelected = !isSelected;
        SetSelect(isSelected);
        OnElementClick?.Invoke(id);
    }

    public void SetSelect(bool isSelected)
    {
        this.isSelected = isSelected;
        background.gameObject.SetActive(isSelected);
    }
}
