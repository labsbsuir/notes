﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Tag
{
    public string hexCode;
    public int id;

    public Tag(string hexCode, int id)
    {
        this.hexCode = hexCode;
        this.id = id;
    }

    public Tag()
    {

    }
}
